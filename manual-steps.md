# Manual steps

1.Open GitBash inside the project folder and start the containers with the command bellow.

    docker-compose up -d

2.Open localhost on your web browser and wait for the installation of Magento. If the installation is completed successfully, Magento luma home page will appear.

3.Copy the code_folder.tar.gz in the Magento container with this command:

    docker cp code_folder.tar.gz magento_conta:/opt/bitnami/magento/htdocs/app

4.Enter the Magento container:

    winpty docker exec -it magento_conta bash

5.Go inside the /app/ folder:

    cd /opt/bitnami/magento/htdocs/app/

6.Extract the code_folder.tar.gz

    tar -xzvf code_folder.tar.gz

7.Go inside the htdocs folder:

    cd /opt/bitnami/magento/htdocs/

8.Unlink /app/etc and copy the unlinked etc files:

    unlink ./app/etc
    cp -r /bitnami/magento/htdocs/app/etc app/etc

9.Enable the module:

    bin/magento module:enable Dejan_NovModul
    bin/magento setup:upgrade
    bin/magento setup:di:compile

10.After the completion of the steps above, open the link below in your browser to see the result.

    http://localhost/dejan/index/index
