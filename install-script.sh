docker-compose up -d

var_website_result=$(curl http://127.0.0.1 | grep -io "Home page" | grep -u "Home" | grep -u "Page")

while [[ $var_website_result != "Home Page" ]]
do
var_website_result=$(curl http://127.0.0.1 | grep -io "Home page" | grep -u "Home" | grep -u "Page")
sleep 3

echo "--------------------------------------------"
echo "Please wait for the magento instalation."
echo "--------------------------------------------"

sleep 3
done

echo "Please wait, installing the custom module ..."

docker cp code_folder.tar.gz magento_conta:/opt/bitnami/magento/htdocs/app
winpty docker exec -it magento_conta bash -c "cd /opt/bitnami/magento/htdocs/app && tar -xzvf code_folder.tar.gz"
winpty docker exec -it magento_conta bash -c "unlink ./opt/bitnami/magento/htdocs/app/etc" 
winpty docker exec -it magento_conta bash -c "cp -r /bitnami/magento/htdocs/app/etc /opt/bitnami/magento/htdocs/app/etc"
winpty docker exec -it magento_conta bash -c "cd /opt/bitnami/magento/htdocs/ && bin/magento module:enable Dejan_NovModul"
winpty docker exec -it magento_conta bash -c "cd /opt/bitnami/magento/htdocs/ && bin/magento setup:upgrade"
winpty docker exec -it magento_conta bash -c "cd /opt/bitnami/magento/htdocs/ && bin/magento setup:di:compile"

sleep 3
echo "-----------------------------------------------------------"

echo "Setup is finished."
echo "Visit http://127.0.0.1/dejan/index/index/"

echo "In order to stop the server and destroy the containers use
docker-compose down -v"

echo "-----------------------------------------------------------"
sleep 7

start http://localhost/dejan/index/index